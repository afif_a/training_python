from django.test import TestCase
from django.contrib.auth import get_user_model

class ModelTest(TestCase):

    def test_create_user_with_email_successful(self):
        """Test createing a new user with email successful"""
        email = 'afif@sekolahmu.co.id'
        password = 'mypass123'
        user = get_user_model().objects.create_user(
                    email=email,
                    password=password
                )

        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        """Test email is normalized"""
        email = "afif@SEKOLAHMU.CO.ID"
        user = get_user_model().objects.create_user(email, 'mypass123')

        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """test invalid email"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'test123')

    def test_create_new_superuser(self):
        """Test creating a new superuser"""
        user = get_user_model().objects.create_superuser(
            'afif@sekolahmu.co.id',
            'test123'
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
